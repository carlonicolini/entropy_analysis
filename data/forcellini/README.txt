Matrici Giulia Forcellini

Ti ho messo le matrici dei preprocessing che ho fatto. In totale sono 33 matrici (11 per 3 gruppi, H, M, L), ogni matrice è la media di 39 soggetti. Ti ho cancellato le matrici vecchie, così usiamo soltanto matrici che abbiano lo stesso preprocessing dell'immagine. 

C'è un problema per tutte e tre le matrici con FIX, non riesco a sparsificarle bene con la percolation e restano iper dense (sul 40%-50%), così facendo non si riesce a calcolarvi la modularità, ne con Newman ne con InfoMap. Probabilmente è un problema di preprocessing dell'immagine, ma non riesco a capire dove possa essere il problema, dato che ho seguito la stessa pipeline per tutti......Come si può fare a vedere se le matrici sono effettivamente patologiche o meno? Avevi accennato qualcosa sul controllare gli autovalori l'altro giorno, giusto?

Mancano ancora le due pipeline con PCA, le farò una volta tornata dalle ferie, che l'ultima volta ho avuto un  po' di problemi per alcuni soggetti, che avevano soltanto 1 o 2 componenti che spiegavano il 50% della varianza, e non è assolutamente in linea con la letteratura.....

Passages:
0 FIX
6 Six movements parameters
8 White matter CSF + six movements parameters
9 White matter CSF + six movements parameters + Global signal regression
24 Volterra
27 Volterra + White matter CSF + Global signal regression
