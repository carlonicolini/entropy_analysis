
Please find attached 3 files:

(1)    controls.mat – 90 x 90 x 60 adjacency matrix for control subjects described here doi:10.1038/srep17755

(2)    demos.mat – includes ids, postmenstrual age at scan, gestational age at birth and number of volumes analyzed (out of 200); the order of the subjects in this file is their order in the connectivity matrix

(3)    maskLabels – the regions of interest used, based on AAL labels in neonates (https://www.med.unc.edu/bric/ideagroup/free-softwares/unc-infant-0-1-2-atlases)


