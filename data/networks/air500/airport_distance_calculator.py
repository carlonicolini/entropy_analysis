import numpy as np
import pandas as pd
import geopy.distance

# Read the airports.dat database as obtained from OpenFlights database
# https://github.com/jpatokal/openflights/
df_air = pd.read_csv('airports.dat',header=-1)
# Here I just changed the IATA code of Hyderabad from HDD to HYD
# and fixed the DOHA missing IATA

Air500names = open('air500names.txt').read().split('\n')[0:-1]
# create the distances matrix, set -1 as default value
dij = -np.ones([len(Air500names),len(Air500names)])

# Check if Air500 list
for s in Air500names:
    if s not in set(df_air[4]):
        print(s,'not existent!')

print(len(dij))
# Create the distance matrix, based on geographical distance in kilometers

for i,airport_iata_i in enumerate(Air500names):
    iata_i = np.where(df_air[4]==airport_iata_i)[0][0]
    print('Progress %.2f' % float(100.0*i/len(dij)) )
    for j,airport_iata_j in enumerate(Air500names):
        if j > i:
            try:
                iata_j = np.where(df_air[4]==airport_iata_j)[0][0]
                coord1 = df_air[6][iata_i], df_air[7][iata_i]
                coord2 = df_air[6][iata_j], df_air[7][iata_j]
                dij[i,j] = geopy.distance.vincenty(coord1,coord2).km
            except:
                pass
dij = dij + dij.T
np.fill_diagonal(dij,0)
np.savetxt('Air500_dist.txt', dij, delimiter=' ')