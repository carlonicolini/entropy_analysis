Folder per analisi con metodo entropie spettrali.

Richiede git-lfs per scaricare l'intero repository.
Richiede `nbstripout` sudo pip3 install nbstripout per pulire i notebooks al commit e non salvare notebooks giganteschi.

Per farlo:
curl -s https://packagecloud.io/install/repositories/github/git-lfs/script.deb.sh | sudo bash
sudo apt-get install git-lfs
git lfs install
