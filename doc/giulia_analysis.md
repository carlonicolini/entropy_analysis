# Guida analisi dati per Giulia

Questa folder contiene i plot dei tuoi dataset raggruppati per pipeline, motion e scrubbing. Il nome file contiene la threshold assoluta utilizzata su tutte le matrici. I file con suffisso 'a' sono le entropie spettrali. I files con suffisso 'b' sono gli istogrammi degli autovalori del laplaciano, i files con suffisso 'c' sono gli istogrammi degli elementi della matrice di adiacenza.

## Installazione
Per potere generare i plot delle entropie spettrali ti servono alcune librerie `python3`.
Per fare questo senza inquinare il tuo PYTHONPATH ti faccio creare un virtualenv

### Installazione di `pip3` e `virtualenv`

Installiamo il python package manager `pip` e l'ambiente `virtualenv`.
   
    sudo apt-get install python3-virtualenv python3-pip


### Installazione di `git-lfs` e download del repository `entropy_analysis`:

Installiamo `git-lfs` che è il sistema che permette di inserire in un repository git dei file binari grandi senza inquinare il repository di commit enormi.

    $> sudo apt-get install software-properties-common
    $> sudo add-apt-repository ppa:git-core/ppa
    $> sudo apt-get update
    $> curl -s https://packagecloud.io/install/repositories/github/git-lfs/script.deb.sh | sudo bash
    $> sudo apt-get install git-lfs
    $> git lfs install

Ora cloniamoci il repository `entropy_analysis` che ho sul mio repository `bitbucket`:

    $> git clone https://bitbucket.org/carlonicolini/entropy_analysis

Dovrebbe metterci un po' di tempo perchè ci sono i dataset dentro anche. La dimensione finale della cartella è di circa 700 mega.
Puoi anche visitare il repository all'indirizzo [https://bitbucket.org/carlonicolini/entropy_analysis](https://bitbucket.org/carlonicolini/entropy_analysis) e scaricarlo come file zip se vuoi altrimenti, però bisogna seguire un workaround per scaricare i files `git-lfs` [come spiegato qui](https://bitbucket.org/carlonicolini/entropy_analysis/downloads/?tab=downloads).


### Creazione di un ambiente `virtualenv` isolato:

Entra nel repository `entropy_analysis` appena scaricato e crea un `virtualenv`


    $> cd entropy_analysis
    $> virtualenv  -p python3 .


Ci mette un pochino ma crea un ambiente python3 dentro la cartella `entropy_analysis`.

### Installazione dei pacchetti python3 necessari:

Sempre rimanendo nella cartella `entropy_analysis`

    pip3 install -U numpy pandas matplotlib scipy bctpy pickle seaborn

### Installazione di networkqit

    Vai nel mio folder `~/Dropbox/Brainetlab/Nicolini/networkqit/`. Copia il file `networkqit-0.12.tar.gz` dentro la cartella `entropy_analysis` che hai scaricato.
    
    Ora puoi installare il mio pacchetto `networkqit` nel virtualenv che hai creato:

    `pip3 install networkqit-0.12.tar.gz`

    Dovrebbe tirarsi giù anche tutte le dipendenze da solo.
    Non usare il `sudo` perchè altrimenti installi nelle dipendenze di sistema, l'idea di `virtualenv` è di avere un ambiente isolato per giocare con le varie librerie e dipendenze.

### Se tutto è andato a buon fine puoi provare a lanciare lo script `FrontiersAnalysis.py` che è dentro la folder `entropy_analysis/notebooks/FrontiersAnalysis.py`.

    $> cd entropy_analysis/notebooks/
    $> python3 FrontiersAnalysis.py

Per ora ho messo le seguenti threshold assolute, se volessi cambiare basta cambiare gli intervalli (vedi riga 342)

    for thresh in np.linspace(0.0,0.5,11):
        ....

Se tutto funziona il file esce senza errori con la scritta "Done."
Questo dovrebbe bastare per generare tutte le figure che hai in questa folder.
Qui di seguito ti allego delle info e commenti sul funzionamento del codice.

### Commenti ulteriori e chiarimento del codice

Il file `FrontiersAnalysis.py` è organizzato in 3 funzioni principali.
Tutto avviene nella funzione `__main__` che è presente alla fine, quindi puoi lanciare le varie funzioni separatamente unicamente commentandole nel `__main__`.
Per le tue analisi, per semplificare, ho implementato la funzione `analyze_forcellini_data` che comprende caricamento, calcolo ed analisi con generazione dei grafici.
Le 3 funzioni principali sono `collect_step`, `compute_step` e `forcellini_plotting`.

La funzione `collect_step` raccoglie i vari dataset che sono presenti nella folder `entropy_analysis/data/` indicizzati per il cognome di chi mi ha dato il dataset. Per cambiare il path basta che cambi la variabile `directories` a linea 156.
Questa funzione basta che la chiami la prima volta per creare il file `data_forcellini_raw.pkl` e poi
una volta salvato la commenti dalla linea dove viene chiamata `collect_step` nella funzione `analyze_forcellini_data` e lanci solo il `compute_step`.

Ovviamente ogni dataset è in formato diverso, ma per gli scopi di uniformare tutto quanto ad uno stesso formato comune ho utilizzato questo schema.
Per ogni ricercatore creo un dizionario `data` che contiene come chiave il cognome del ricercatore e come valore il campo `data` che è una lista vuota, alla quale mano a mano andrò ad aggiungere elementi.
Ogni elemento della lista rappresenta la riga di una tabella, dove le colonne identificano i campi.
Sostanzialmente la lista `data['forcellini']['data']` è una lista di dizionari, dove i campi principali comuni a tutti sono:

    1. `matrix` che rappresenta il dato grezzo
    2. `info` che contiene informazioni generali, grezze.

Solo alla fine quando il dizionario `data` è completamente riempito dei suoi campi ottenuti da lettura dei dataset grezzi, questo viene salvato su disco con il metodo `pickle.dump`, che è simile al comando `save` di `matlab`.

E' solo con la funzione `compute_step` che ai campi `matrix` e `info` vengono aggiunti anche altri campi come `eigs_l` che rappresenta gli autovalori del laplaciano oppure il calcolo vero e proprio delle entropie tramite `beta_range` ed `entropy`.
Nella funzione `compute_step` devi specificare il range dei valori di `beta` rispetto ai quali vuoi plottare le curve di entropia spettrale (`np.logspace(-3, 3, 1000)` vuol dire da 10^-3 a 10^3 con 1000 passi).
Infine quello che viene salvato è la variabile `dfs` come dizionario con chiavi i cognomi (ad esempio `forcellini` ) e come valori dei `Dataframe` pandas creati alla riga `dfs[researcher] = pd.DataFrame(data[researcher]['data'])`

Alla fine la struttura del dato che viene salvato con `pickle.dump` è il seguente:

dfs[researcher]
              |--->dataframe
                            |-> matrix
                            |-> info
                            |-> eig_l
                            |-> entropy
                            |-> beta_range

Quando lanci `compute_step` questo stesso dato viene caricato con `pickle.load` (specificando il nome file corretto) e vengono aggiunti altri campi. Ho fatto anche un trucchetto per applicare uniformemente la stessa trasformazione alla matrice `'matrix'`. Alla funzione `compute_step` devi passare una funzione che definisci tu che prende come input una matrice e restituisce una matrice (esempio poco sotto).

    data[researcher]['data'][i]['A'] = A
    data[researcher]['data'][i]['beta_range'] = beta_range
    data[researcher]['data'][i]['eigs_l'] = eigvalsh(L)
    #data[researcher]['data'][i]['eigs_sl'] = eigvalsh(SL)
    #data[researcher]['data'][i]['eigs_nl'] = eigvalsh(NL)
    data[researcher]['data'][i]['all_a'] = A.flatten()[np.nonzero(A.flatten())]
    data[researcher]['data'][i]['entropy'] = nq.batch_compute_vonneumann_entropy(L=L, beta_range=beta_range)

A cosa serve tutto questo? A facilitare la visualizzazione e la manipolazione dei dati in maniera uniforme e con poche righe di codice, il piccolo overhead di sopra è necessario per essere liberi dopo.
Quello che manca infatti è la funzione di visualizzazione `data_grid_plot`.
Essa accetta come input i seguenti argomenti:

    def data_grid_plot(df, x, y, rows, cols, hue, plot=plt.semilogx, **kwargs):

Il primo argomento è un `pandas` `Dataframe` e cioè quello che ottieni come valore di `dfs` alla chiave `"forcellini"`. Gli argomenti `x,y` invece sono quale variabile vuoi mettere sulle `x` e sulle `y` dei grafici. Infine le variabili `rows,cols,hue` indicando rispetto a quali campi aggiuntivi del dataframe vuoi raggruppare il tuoi plot.