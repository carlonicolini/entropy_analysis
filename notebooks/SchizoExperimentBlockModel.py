import gzip
import functools
import time
from multiprocessing import Pool, cpu_count
from GraphToolHelper import add_edge_gradient_property, add_vertex_membership_property, add_vertex_names, circo_edge_bundle, to_graph_tool
from GraphPlot import draw_network, heatmap, annotate_heatmap
from FrontiersAnalysis import data_grid_plot, analyze_forcellini_data, forcellini_plotting, compute_step, collect_step
import graph_tool.all as gt
import pickle
from FrontiersAnalysis import data_grid_plot
import matplotlib.pyplot as plt
import seaborn as sns
import bct
import networkx as nx
import networkqit as nq
from scipy.io import loadmat
import scipy
import pandas as pd
import numpy as np
import sys
import os
from os.path import expanduser
home = expanduser("~")
sys.path.append(home + '/workspace/networkqit')
plt.style.use('ggplot')

# Additional utility functions
from tqdm import tqdm

gt.openmp_set_num_threads(2)  # to exploit better

def optimize_blocks_thread(B, g):
    reps = 50
    xij = g.ep.weight.copy()  # original edge covariate xij, make a deep copy
    yij = xij  # store a temporary
    yij.a = np.log(xij.a)  # apply the transformation
    states = [gt.minimize_blockmodel_dl(g, B_min=B, B_max=B, state_args=dict(
        recs=[g.ep.weight], rec_types=["real-normal"]), deg_corr=True) for i in range(reps)]
    return np.array(list(states[np.argmin([state.entropy() for state in states])].get_blocks())), states[np.argmin([state.entropy() for state in states])].entropy()+np.log(g.ep.weight.a).sum(), B


def exp1()
:    T = []
    df = None
    blocks = range(4, 11, 1)
    pool = Pool(16)
    for thresh in tqdm(np.linspace(0.025, 0.25, 10)):
        thresh_str = '%.4f' % thresh
        del df
        df = pickle.load(open('data_schizo_fisher_averages_thresh_' + thresh_str + '.pkl', 'rb'))['bordier']
        for condition in tqdm(['control', 'patient']):
            W = df[ df.condition == condition]['A'].iloc[0]
            g = to_graph_tool(W)
            t0 = time.time()
            res = pool.map(functools.partial(optimize_blocks_thread, g=g), blocks)
            #print('Elapsed inference all blocks = %.2f seconds' % (time.time()-t0))
            for block in tqdm(range(len(blocks))):
                memb, quality, B = res[block]
                memb = nq.reindex_membership(memb, compress_singletons=True)
                memb_comm_weight = nq.reindex_membership(memb, key='community_weight', adj=W)
                ersw, ersnormw = nq.comm_mat(W, memb)
                ers, ersnorm = nq.comm_mat((W > 0).astype(float), memb)
                erscommw, ersnormcommw = nq.comm_mat(W, memb_comm_weight)
                T.append({'condition': condition,
                          'thresh': thresh_str,
                          'W': W,
                          'B': B,
                          'memb': memb,
                          'entropy': quality,
                          'ersw': ersw,
                          'ersnormw': ersnormw,
                          'ers': ers,
                          'ersnorm': ersnorm,
                          'erscommw': erscommw,
                          'ersnormcommw': ersnormcommw
                          })
            dfinfo = pd.DataFrame(T)
            pickle.dump(dfinfo, gzip.open('output/processed/schizo_wsbm_dfinfo_lognormal.pkl.gz', "wb"), protocol=-1)
    dfinfo = pd.DataFrame(T)
    pickle.dump(dfinfo, gzip.open('output/processed/schizo_wsbm_dfinfo_lognormal.pkl.gz', "wb"), protocol=-1)

if __name__=='__main__':
    exp1()
