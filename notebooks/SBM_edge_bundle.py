import numpy as np
import scipy
from scipy.io import loadmat
import bct
import matplotlib.pyplot as plt
import re
plt.style.use('ggplot')
from FrontiersAnalysis import data_grid_plot
import graph_tool.all as gt

plt.switch_backend('cairo')

import pickle
dfs = pickle.load(open('data_forcellini_thresh_0.4000.pkl', 'rb'))
df_forcellini = dfs['forcellini']
del dfs

def to_graph_tool(adj):
    g = gt.Graph(directed=False)
    eprop = g.new_edge_property('double')
    g.edge_properties['weight'] = eprop
    nnz = np.nonzero(np.triu(adj))
    g.add_edge_list(np.hstack([np.transpose(nnz),adj[nnz[0]]]),eprops=[eprop])
    return g


import pickle
dfs = pickle.load(open('data_forcellini_thresh_0.3250.pkl', 'rb'))
df_forcellini = dfs['forcellini']
del dfs
A = df_forcellini[(df_forcellini['passages']==27) & (df_forcellini['scrubbing']==0) & (df_forcellini['motion']=='L')]['A'].iloc[0]
g = to_graph_tool(bct.threshold_absolute(A,0.5)).copy()
XYZ=np.loadtxt('/home/carlo/workspace/communityalg/data/Crossley_coords_mni.txt', delimiter=',')
print(g.num_vertices(),g.num_edges())
posXY = gt.sfdp_layout(g)
posXZ = gt.sfdp_layout(g)
for i,x in enumerate(posXY):
    posXY[i] = XYZ[i,0:2]
    posXZ[i] = -XYZ[i,1:]

state = gt.minimize_nested_blockmodel_dl(g,state_args=dict(recs=[g.ep.weight],rec_types=["real-exponential"]), deg_corr=True)

fig,ax = plt.subplots(ncols=2, nrows=1,figsize=(12,8))
ax[0].grid(False)
ax[1].grid(False)

deg = g.degree_property_map("in")
t = gt.get_hierarchy_tree(state)[0]
ctsXY = gt.get_hierarchy_control_points(g, t, posXY, beta=0.15)
ctsXZ = gt.get_hierarchy_control_points(g, t, posXZ, beta=0.15)
gt.graph_draw(g,
              pos=posXY,
              edge_color=gt.prop_to_size(g.ep.weight, power=0.5, log=False),
              vertex_anchor=0,
              vertex_size=deg,
              eorder=g.ep.weight,
              edge_pen_width=gt.prop_to_size(g.ep.weight, 0.1, 2, power=0.5, log=False),
              ecmap=(plt.cm.viridis, 0.5),
              vertex_fill_color=state.levels[0].b,
              vcmap = (plt.cm.Set3, 1),
              edge_control_points=ctsXY,
              mplfig = ax[0])

gt.graph_draw(g,
              pos=posXZ,
              edge_color=gt.prop_to_size(g.ep.weight, power=0.5, log=False),
              vertex_anchor=0,
              vertex_size=deg,
              eorder=g.ep.weight,
              edge_pen_width=gt.prop_to_size(g.ep.weight, 0.1, 2, power=0.5, log=False),
              ecmap=(plt.cm.viridis, 0.5),
              vertex_fill_color=state.levels[0].b,
              vcmap = (plt.cm.Set3, 1),
              edge_control_points=ctsXZ,
              mplfig = ax[1])

ax[0].axis('off')
ax[1].axis('off')
plt.pause(1)
plt.show()