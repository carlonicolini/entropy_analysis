#!/usr/bin/python3
import numpy as np
import networkx as nx
import matplotlib.pyplot as plt

def draw_network(graph, nodes_membership, **kwargs):
    ''' Draw the graph with its labels '''
    plot_prop = {}
    plot_prop['nodes_size'] = kwargs.get('nodes_size', 10)
    plot_prop['edges_width'] = kwargs.get('edges_width', 1)
    plot_prop['color_map'] = kwargs.get('color_map', plt.cm.Spectral)
    plot_prop['font_size'] = kwargs.get('font_size', 15)
    plot_prop['layout_method'] = kwargs.get('layout_method', 'neato')
    plot_prop['node_alpha'] = kwargs.get('node_alpha', 0.8)
    plot_prop['draw_edges'] = kwargs.get('draw_edges', False)
    plot_prop['draw_edges_weights'] = kwargs.get('draw_edges_weights', False)
    plot_prop['axisX'] = kwargs.get('axisX', 0)
    plot_prop['axisY'] = kwargs.get('axisY', 2)
    plot_prop['mst_sparsification'] = kwargs.get('mst_sparsification', False)
    plot_prop['highlight_comm'] = kwargs.get('highlight_comm', None)
    plot_prop['ax'] = kwargs.get('ax',None)

    original_weights_matrix = kwargs.get('original_weights_matrix', None)
    split_hemispheres = kwargs.get('split_hemispheres', None)
    output_file_name = kwargs.get('output_file_name', None)
    node_pos = kwargs.get('node_pos', None)

    if node_pos is None:
        node_pos = nx.graphviz_layout(graph, prog=plot_prop['layout_method'])
    elif isinstance(node_pos, str):
        #logging.info('Ignoring layout_method, using loaded node coordinates')
        node_pos = load_node_positions_2D(node_pos)
    else:
        if np.shape(node_pos)[1] == 3:
            #logging.info('Ignoring layout_method, using provided 3D node coordinates')
            node_pos_dict = {}
            for i, r in enumerate(node_pos):
                node_pos_dict[i] = node_pos[i, plot_prop['axisY']], node_pos[i, plot_prop['axisX']] 
            node_pos = node_pos_dict.copy()

    # Nodewise size
    nodesize = [plot_prop['nodes_size'] / 25.0 *
                (graph.degree(n) + 1) for n in graph.nodes()]
    nodesize = [plot_prop['nodes_size'] * 2.5 * (float(nodes_membership.get(
        n, False) == plot_prop['highlight_comm']) + 0.25) for n in graph.nodes()]

    nodes_membership_highlight = {}
    for n, c in nodes_membership.items():
        if c == plot_prop['highlight_comm']:
            nodes_membership_highlight[n] = 1
        else:
            nodes_membership_highlight[n] = 0

    edges_width = None  # [1 for (n,m,w) in G.edges(data=True)]
    edges_color = None  # [1 for (n,m,w) in G.edges(data=True)]
    if not hasattr(graph, 'is_weighted'):
        graph.is_weighted = False

    if graph.is_weighted:
        edges_width = [plot_prop['edges_width'] *
                       np.log(2 * w['weight'] * 2 + 1)
                       for (n, m, w) in graph.edges(data=True)]

        edges_color = [plot_prop['edges_width'] *
                       np.log(2 * w['weight'] + 1)
                       for (n, m, w) in graph.edges(data=True)]
    else:
        edges_width = [plot_prop['edges_width'] * (graph.degree(n)
                                                   + graph.degree(m)) / 2 for (n, m, w) in graph.edges(data=True)]

        edges_color = [(graph.degree(n) + graph.degree(m)) / 2 + 5
                       for (n, m, w) in graph.edges(data=True)]
        # Inverted dictionary of nodes is used to lookup dataF
        if original_weights_matrix is not None:
            edges_color = [original_weights_matrix[graph.row_node_dictionary[n]]
                           [graph.row_node_dictionary[m]]
                           for (n, m, w) in graph.edges(data=True)]

    #edges_width = plot_prop['edges_width']
    def community_subgraphs(graph, membership):
        ds = {}
        for u, v in membership.items():
            if v not in ds.keys():
                ds[v] = []
            ds[v].append(u)
        for nbunch in ds.values():
            yield nx.subgraph(graph, nbunch)
        
    if split_hemispheres:
        posShifted = {}
        for regionName, regionPosition in node_pos.iteritems():
            if regionName[-1] == 'L':
                posShifted[regionName] = np.array(
                    regionPosition) - np.array([400, 0])
            if regionName[-1] == 'R':
                posShifted[regionName] = np.array(
                    regionPosition) + np.array([400, 0])

        node_pos = copy(posShifted)
    #fig = plt.figure(figsize=kwargs.get('figsize',(12,8)))
    #ax = plt.gca()
    ax = plot_prop['ax']
    if plot_prop['draw_edges'] is True:
        if plot_prop['mst_sparsification'] is False:
            nx.draw_networkx_edges(graph, node_pos, alpha=0.3, width=edges_width,
                                   edge_cmap=plt.cm.gist_yarg, edge_vmin=np.min(
                                       edges_color) - 0.1,
                                   edge_vmax=np.max(edges_color), with_labels=False, ax=ax)
        else:
            el = list(community_subgraphs(graph, nodes_membership))[
                plot_prop['highlight_comm']]
            nx.draw_networkx_edges(nx.minimum_spanning_tree(el), node_pos, alpha=0.3, width=edges_width,
                                   edge_cmap=plt.cm.Spectral, edge_vmin=np.min(
                                       edges_color) - 0.1,
                                   edge_vmax=np.max(edges_color), with_labels=False, ax=ax)

    if plot_prop['draw_edges_weights'] is True:
        weight_labels = dict(((u, v), '%.3f' % d.get('weight', 1))  
                             for u, v, d in graph.edges(data=True))
        #nx.draw_networkx_edge_labels(graph, node_pos, edge_labels=weight_labels, ax=ax)
    
    nx.draw_networkx_nodes(graph, node_pos, linewidths=0.2,
                           node_size=nodesize,
                           node_color=list(nodes_membership.values()),
                           alpha=plot_prop['node_alpha'],
                           cmap=plot_prop['color_map'],
                           ax=ax)

    if kwargs.get('draw_labels', False):
        node_pos_shift = {}
        for key, val in node_pos.iteritems():
            node_pos_shift[key] = (val[0], val[1] + plot_prop['font_size'] / 2)
        nx.draw_networkx_labels(graph, node_pos_shift, font_size=plot_prop['font_size'], ax=ax)
    
    num_comms = len(np.unique(list(nodes_membership.values())))
    title = '|E|=%.2f  |C|=%d Q=%.3f' % (graph.number_of_edges(),num_comms,kwargs.get('Q',-1))
    ax.set_title(title)  # + ' Method= ' + measureNodeColor )
    ax.xaxis.set_major_locator(plt.NullLocator())
    ax.yaxis.set_major_locator(plt.NullLocator())
        
    #if output_file_name is not None:
    #    plt.savefig(output_file_name)

def heatmap(data, row_labels, col_labels, ax=None,
            cbar_kw={}, cbarlabel="", **kwargs):
    """
    Create a heatmap from a numpy array and two lists of labels.

    Arguments:
        data       : A 2D numpy array of shape (N,M)
        row_labels : A list or array of length N with the labels
                     for the rows
        col_labels : A list or array of length M with the labels
                     for the columns
    Optional arguments:
        ax         : A matplotlib.axes.Axes instance to which the heatmap
                     is plotted. If not provided, use current axes or
                     create a new one.
        cbar_kw    : A dictionary with arguments to
                     :meth:`matplotlib.Figure.colorbar`.
        cbarlabel  : The label for the colorbar
    All other arguments are directly passed on to the imshow call.
    """

    if not ax:
        ax = plt.gca()

    # Plot the heatmap
    im = ax.imshow(data, **kwargs)
    #plt.clim(0,1)

    # Create colorbar
    cbar = ax.figure.colorbar(im, ax=ax, **cbar_kw)
    cbar.ax.set_ylabel(cbarlabel, rotation=-90, va="bottom")

    # We want to show all ticks...
    ax.set_xticks(np.arange(data.shape[1]))
    ax.set_yticks(np.arange(data.shape[0]))
    # ... and label them with the respective list entries.
    ax.set_xticklabels(col_labels)
    ax.set_yticklabels(row_labels)

    # Let the horizontal axes labeling appear on top.
    ax.tick_params(top=True, bottom=False,
                   labeltop=True, labelbottom=False)

    # Rotate the tick labels and set their alignment.
    plt.setp(ax.get_xticklabels(), rotation=-30, ha="right",
             rotation_mode="anchor")

    # Turn spines off and create white grid.
    for edge, spine in ax.spines.items():
        spine.set_visible(False)

    ax.set_xticks(np.arange(data.shape[1]+1)-.5, minor=True)
    ax.set_yticks(np.arange(data.shape[0]+1)-.5, minor=True)
    ax.grid(False)
    ax.tick_params(which="major", bottom=False, left=False)

    return im, cbar


def annotate_heatmap(im, data=None, valfmt="{x:.2f}",
                     textcolors=["white", "black"],
                     threshold=None, **textkw):
    """
    A function to annotate a heatmap.

    Arguments:
        im         : The AxesImage to be labeled.
    Optional arguments:
        data       : Data used to annotate. If None, the image's data is used.
        valfmt     : The format of the annotations inside the heatmap.
                     This should either use the string format method, e.g.
                     "$ {x:.2f}", or be a :class:`matplotlib.ticker.Formatter`.
        textcolors : A list or array of two color specifications. The first is
                     used for values below a threshold, the second for those
                     above.
        threshold  : Value in data units according to which the colors from
                     textcolors are applied. If None (the default) uses the
                     middle of the colormap as separation.

    Further arguments are passed on to the created text labels.
    """

    if not isinstance(data, (list, np.ndarray)):
        data = im.get_array()

    # Normalize the threshold to the images color range.
    if threshold is not None:
        threshold = im.norm(threshold)
    else:
        threshold = im.norm(data.max())/2.

    # Set default alignment to center, but allow it to be
    # overwritten by textkw.
    kw = dict(horizontalalignment="center",
              verticalalignment="center")
    kw.update(textkw)

    # Get the formatter in case a string is supplied
    if isinstance(valfmt, str):
        import matplotlib as mpl
        valfmt = mpl.ticker.StrMethodFormatter(valfmt)

    # Loop over the data and create a `Text` for each "pixel".
    # Change the text's color depending on the data.
    texts = []
    for i in range(data.shape[0]):
        for j in range(data.shape[1]):
            kw.update(color=textcolors[im.norm(data[i, j]) > threshold])
            text = im.axes.text(j, i, valfmt(data[i, j], None), **kw)
            texts.append(text)

    return texts

def demo1():
    import pickle
    import bct
    dfs = pickle.load(open('output/processed/data_forcellini_thresh_0.0000.pkl', 'rb'))
    df_forcellini = dfs['forcellini']
    del dfs
    XYZ = np.loadtxt('/home/carlo/workspace/communityalg/data/Crossley_coords_mni.txt', delimiter=',')
    A = df_forcellini[(df_forcellini['passages']==8) & (df_forcellini['scrubbing']==0) & (df_forcellini['motion']=='L')]['A'].iloc[0]
    g = to_graph_tool(bct.threshold_proportional(A,0.01)).copy()
    memb,q = bct.community_louvain(A)
    #memb,q = bct.community_louvain(A,B='negative_asym')
    fig,ax = plt.subplots(ncols=2,nrows=1,figsize=(10,5))
    circo_edge_bundle(g,memb,colormap=plt.cm.get_cmap('viridis'))
    circo_edge_bundle(g,memb,colormap=plt.cm.get_cmap('viridis'),ax=ax[1])
    plt.savefig('ciccio.pdf')
    plt.show()

def demo2():
    memb,_ = bct.community_louvain(A)
    memb = np.array(reindex_membership(memb)).astype(int)
    g = add_vertex_membership_property(g, memb, color=memb, colormap=cmap)
    g = add_edge_gradient_property(g, memb, cmap)
    g = add_vertex_names(g)

if __name__=='__main__':
    demo1()

