#!/usr/bin/python3
import os
from os.path import expanduser
home = expanduser("~")
import sys
sys.path.append(home + '/workspace/networkqit')
import networkqit as nq
import numpy as np
import pandas as pd
import scipy
from scipy.io import loadmat
import networkx as nx
import pickle
import graph_tool.all as gt
# Additional utility functions
from GraphToolHelper import to_graph_tool
import pickle
import gzip
from tqdm import tqdm
from functools import partial
from itertools import product
from multiprocessing import Pool, cpu_count
import parmap
import logging
logging.basicConfig(level=logging.INFO)
logger = logging.getLogger(__name__)
from time import gmtime, strftime

def optimize_passage_motion(*args, **kwargs):
    (passages, motion, B, thresh) = args
    thresh_str = '%.4f' % thresh
    all_g = kwargs['all_g']
    g = all_g[(thresh, passages, motion)]
    W = gt.adjacency(g,weight=g.ep.weight).toarray()
    #W = all_W[(thresh, passages, motion)]
    xij = g.ep.weight.copy()  # original edge covariate xij, make a deep copy
    yij = xij  # store a temporary
    yij.a = np.log(xij.a)  # apply the transformation
    state = gt.minimize_blockmodel_dl(g, B_min=B, B_max=B, state_args=dict(recs=[g.ep.weight], rec_types=["real-normal"]), deg_corr=True)
    memb = list(state.get_blocks())
    quality = float(state.entropy() + np.log(g.ep.weight.a).sum())
    #memb, quality = np.random.randint(1, 10, [638, ]), 0.5
    memb = nq.reindex_membership(memb, compress_singletons=True)
    memb_comm_weight = nq.reindex_membership(memb, key='community_weight', adj=W)
    ersw, ersnormw = nq.comm_mat(W, memb)
    ers, ersnorm = nq.comm_mat((W > 0).astype(float), memb)
    erscommw, ersnormcommw = nq.comm_mat(W, memb_comm_weight)
    out = {'passages': passages,
           'thresh': thresh_str,
           'motion': motion,
           'W': W,
           'B': B,
           'memb': memb,
           'entropy': quality,
           'ersw': ersw,
           'ersnormw': ersnormw,
           'ers': ers,
           'ersnorm': ersnorm,
           'erscommw': erscommw,
           'ersnormcommw': ersnormcommw
           }
    return out

def run_subject(prefix, subject):
    logger.info('Subject %s analysis started at %s' % (str(subject),strftime("%Y-%m-%d %H:%M:%S", gmtime()) ))
    T = []
    passages = [0, 1, 5, 6, 8, 9, 24, 27, 85]
    motion = ['L', 'M', 'H']
    blocks = [4, 5]
    thresholds = np.linspace(0.025, 0.25, 10)
    # Preload the matrix W and the graph g into separate dictionaries
    # This is done to save reading from disk multiple times
    all_g = {}
    for thresh in thresholds:
        thresh_str = '%.4f' % thresh
        df = pickle.load(open(prefix + thresh_str + '.pkl', 'rb'))['forcellini']
        for passage in passages:
            for mot in motion:
                W = df[np.logical_and(passage == df.passages, df.motion == mot)]['A'].iloc[0]
                g = to_graph_tool(W)
                all_g[(thresh, passage, mot)] = g
    del df  # to save memory
    experiment = list(product(passages, motion, blocks, thresholds))
    logger.info('Subject %s parameters generation started at %s' % (str(subject),strftime("%Y-%m-%d %H:%M:%S", gmtime()) ))
    logger.info('Subject %s parallel processing started at %s' % (str(subject),strftime("%Y-%m-%d %H:%M:%S", gmtime()) ))
    # from contextlib import closing # in this way memory usage is limited
    # with closing( Pool(cpu_count()//2) ) as pool:
    #     res = parmap.starmap(partial(optimize_passage_motion, all_g=all_g), experiment, pm_pbar=True, pm_pool=pool)
    res = parmap.starmap(partial(optimize_passage_motion, all_g=all_g), experiment, pm_processes=32, pm_chunksize=1, pm_pbar=True)
    logger.info('Subject %s parallel processing finished at %s' % (str(subject),strftime("%Y-%m-%d %H:%M:%S", gmtime()) ))
    logger.info('Subject %s file dump started at %s' % (str(subject),strftime("%Y-%m-%d %H:%M:%S", gmtime()) ))
    pickle.dump(res, gzip.open('output/2019/processed/subjwise/wsbm_dfinfo_lognormal_subj_%d.pkl.gz' % subj, 'wb'), protocol=-1)
    logger.info('Subject %s finished at %s' % (str(subject),strftime("%Y-%m-%d %H:%M:%S", gmtime()) ))

if __name__ == '__main__':
    gt.openmp_set_num_threads(1)
    for subj in range(39):
        run_subject('output/2019/processed/subjwise/data_subj_%d_thresh_' % subj, subj)
