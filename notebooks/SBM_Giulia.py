
# coding: utf-8

# In[1]:

import os
import sys
sys.path.append('/home/carlo/workspace/networkqit')
import numpy as np
import pandas as pd
import scipy
from scipy.io import loadmat
import networkqit as nq
from networkqit.utils.visualization import step_callback, plot_spectral_entropy
import multiprocessing
import networkx as nx
import bct
import seaborn as sns
import matplotlib.pyplot as plt
import re
plt.style.use('ggplot')
from os.path import expanduser
home = expanduser("~")
from FrontiersAnalysis import data_grid_plot
from networkqit import sbm, wsbm


# In[2]:


import pickle
dfs = pickle.load(open('data_thresh.pkl', 'rb'))
df_forcellini = dfs['forcellini']
del dfs


# In[3]:


A = df_forcellini[(df_forcellini['passages']==8) & (df_forcellini['scrubbing']==1) & (df_forcellini['filtering']=='M')]['A'].iloc[0]


# In[9]:


import graph_tool.all as gt
def to_graph_tool(adj):
    g = gt.Graph(directed=False)
    eprop = g.new_edge_property('double')
    g.edge_properties['weight'] = eprop
    nnz = np.nonzero(np.triu(adj))
    g.add_edge_list(np.hstack([np.transpose(nnz),adj[nnz[0]]]),eprops=[eprop])
    return g


# In[10]:


A=bct.threshold_absolute(A,0.5)
g = to_graph_tool(A).copy()
print(g.num_vertices(),g.num_edges())

state = gt.minimize_nested_blockmodel_dl(g,state_args=dict(recs=[g.ep.weight],rec_types=["real-exponential"]),deg_corr=True)
print('Done')
state.draw(edge_color=gt.prop_to_size(g.ep.weight, power=1, log=False), ecmap=(plt.cm.inferno, .6),
           eorder=g.ep.weight, edge_pen_width=gt.prop_to_size(g.ep.weight, 1, 4, power=1, log=False),
           edge_gradient=[])


# In[ ]:


bs = state.get_bs()                     # Get hierarchical partition.
bs += [np.zeros(1)] * (10 - len(bs))    # Augment it to L = 10 with
                                        # single-group levels.
state = state.copy(bs=bs, sampling=True)


# In[8]:


ci = list(state.get_bs()[0])
ci = np.array(nq.reindex_membership(ci))
ers,_ = nq.comm_mat(A,ci)
nr = [np.sum(ci==c) for c in np.unique(ci)]
nrns = np.reshape(np.kron(nr,nr),[len(nr)]*2)
As = wsbm(ers,nr,scipy.random.exponential)
Ast = As
bounds,ixes=bct.grid_communities(ci)
plt.figure(figsize=(16,8))
plt.subplot(1,3,1)
plt.imshow(Ast)
plt.title('WSBM matrix')
plt.grid(False)
plt.colorbar(fraction=0.046, pad=0.04)
plt.subplot(1,3,2)
plt.imshow(A[np.ix_(ixes,ixes)])
plt.title('Original matrix')
plt.grid(False)
plt.colorbar(fraction=0.046, pad=0.04)
plt.subplot(1,3,3)
plt.imshow(nq.comm_mat(A,ci)[0]/nrns,interpolation='none')
plt.colorbar(fraction=0.046, pad=0.04)
plt.title('Block matrix')
plt.grid(False)
plt.xticks([]),plt.yticks([])

#np.savetxt('ers.txt',ers)
#np.savetxt('nr.txt',nr)
print('Original num links=%g WSBM num links=%g' % (A[np.nonzero(A)[0]].sum(),Ast[np.nonzero(Ast)[0]].sum()))

plt.figure(figsize=(16,4))
plt.subplot(1,2,1)
sns.distplot(A.flatten()[np.nonzero(A.flatten())[0]])
sns.distplot(Ast.flatten()[np.nonzero(Ast.flatten())[0]])
plt.legend(['original','wsbm'])
plt.subplot(1,2,2)
beta_range=np.logspace(-3,3,1000)
plt.semilogx(beta_range,nq.batch_compute_vonneumann_entropy(nq.graph_laplacian(A),beta_range))
plt.semilogx(beta_range,nq.batch_compute_vonneumann_entropy(nq.graph_laplacian(Ast),beta_range))
plt.legend(['original','wsbm'])
plt.show()

