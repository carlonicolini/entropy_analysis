import os
from os.path import expanduser
home = expanduser("~")
import sys
sys.path.append(home + '/workspace/networkqit')
import numpy as np
import pandas as pd
import scipy
from scipy.io import loadmat
import networkqit as nq
import networkx as nx
import bct
import seaborn as sns
import matplotlib.pyplot as plt
plt.style.use('ggplot')

from FrontiersAnalysis import data_grid_plot
import pickle

import graph_tool.all as gt
# Additional utility functions
from FrontiersAnalysis import data_grid_plot, analyze_forcellini_data, forcellini_plotting, compute_step, collect_step
from GraphPlot import draw_network, heatmap, annotate_heatmap
from GraphToolHelper import add_edge_gradient_property, add_vertex_membership_property, add_vertex_names, circo_edge_bundle, to_graph_tool

from multiprocessing import Pool, cpu_count
import pickle
import time
import progressbar
gt.openmp_set_num_threads(8) # to exploit better 
import functools

def optimize_blocks_thread(B,g):
    reps = 10
    xij = g.ep.weight.copy() # original edge covariate xij, make a deep copy
    yij = xij # store a temporary
    yij.a = np.log(xij.a) # apply the transformation
    states = [gt.minimize_blockmodel_dl(g, B_min=B, B_max=B, state_args=dict(recs=[g.ep.weight],rec_types=["real-normal"]), deg_corr=True) for i in range(reps)]
    return np.array(list(states[np.argmin([state.entropy() for state in states])].get_blocks())), states[np.argmin([state.entropy() for state in states])].entropy()+np.log(g.ep.weight.a).sum(),B

def exp():
    T = []
    df = None
    blocks = range(4,11,1)
    pool = Pool(cpu_count())
    #for thresh in progressbar.progressbar(np.linspace(0.025,0.25,10)):
     #   thresh_str = '%.4f' % thresh
    del df
    df = pickle.load(open('data_forcellini_thresh_percolation.pkl','rb'))
    for passages in pd.unique(df.passages):
        for motion in pd.unique(df.motion):
            thresh=df[(df['motion']==motion) & (df['passages']==passages)]['thresh'].iloc[0]
            print('thresh=%f passages=%s motion=%s'% (thresh,passages,motion))
            W = df[(df['motion']==motion) & (df['passages']==passages)]['spars'].iloc[0]
            g = to_graph_tool(W)
            t0 = time.time()
            res = pool.map(functools.partial(optimize_blocks_thread, g=g), blocks)
            print('Elapsed inference all blocks = %.2f seconds' % (time.time()-t0))
            for block in range(len(blocks)):
                memb, quality, B = res[block]
                memb = nq.reindex_membership(memb,compress_singletons=True)
                memb_comm_weight = nq.reindex_membership(memb, key='community_weight',adj=W)
                ersw, ersnormw = nq.comm_mat(W, memb)
                ers, ersnorm = nq.comm_mat((W>0).astype(float), memb)
                erscommw, ersnormcommw = nq.comm_mat(W, memb_comm_weight)
                T.append({'passages':str(passages),
                          'thresh':thresh,
                          'motion':motion,
                          'W':W,
                          'B':B,
                          'memb':memb,
                          'entropy':quality,
                          'ersw': ersw,
                          'ersnormw':ersnormw,
                          'ers': ers,
                          'ersnorm':ersnorm,
                          'erscommw':erscommw,
                          'ersnormcommw':ersnormcommw
                          })
            dfinfo = pd.DataFrame(T)
            pickle.dump(dfinfo, open('output/processed/wsbm_dfinfo_lognormal_percolation.pkl', "wb"), protocol=-1)
    dfinfo = pd.DataFrame(T)
    pickle.dump(dfinfo, open('output/processed/wsbm_dfinfo_lognormal_percolation.pkl', "wb"), protocol=-1)

if __name__=='__main__':
    exp1()