#!/usr/bin/python3
import os
from os.path import expanduser
home = expanduser("~")
import sys
sys.path.append(home + '/workspace/networkqit')

# This is to set a single thread to each scipy optimization worker
os.environ["MKL_NUM_THREADS"] = "1"
os.environ["NUMEXPR_NUM_THREADS"] = "1"
os.environ["OMP_NUM_THREADS"] = "1"

from itertools import product
from functools import partial
from multiprocessing import Pool, cpu_count
import parmap
import logging
import gzip
import pickle
import bct
import pandas as pd
import numpy as np
import sys
import networkqit as nq
import matplotlib.colors as colors
import matplotlib.pyplot as plt
import seaborn as sns

import logging
logger = logging.getLogger()
logger.setLevel(logging.INFO)

def slice_dataframe(d,passage, motion, key):
    return d[(d['passages']==passage) & (d['motion'] == motion)][key].values[0]

def compute_dkl(*args, **kwargs):
    (passage, motion, t) = args
    A = slice_dataframe(df,passage,motion,'A')
    A = A*(A>=0)
    if np.any((A<0).flatten()):
        logger.warning('negative values in matrix A')
    At = bct.threshold_proportional(A, t)
    #At = bct.threshold_absolute(A, t)
    sol = nq.CWTERG(N=len(A), threshold=At[np.nonzero(At)].min()).fit(G=At, ftol=1E-16)

    # Keep only networks with one connected component
    count = 0
    while True:
        Amodel = np.squeeze(nq.CWTERG(N=len(A), threshold=At[np.nonzero(At)].min()).sample_adjacency(sol['x'], batch_size=1))
        if int(bct.number_of_components(Amodel)) == 1:
            break
        if count > 500:
            logger.warning('reached maximum number of iterations for passage %d motion %s threshold %f' % (passage,motion,t) )
            break
        count += 1
    Lmodel = np.expand_dims(nq.graph_laplacian(Amodel),0)
    L = nq.graph_laplacian(At)
    
    beta_range = kwargs['beta_range']
    Smodel = nq.batch_compute_vonneumann_entropy(L=np.squeeze(Lmodel), beta_range=beta_range)
    Sdata = nq.batch_compute_vonneumann_entropy(L=L, beta_range=beta_range)
    dkl = nq.batch_beta_relative_entropy(Lobs=L, Lmodel=Lmodel, beta_range=beta_range)

    return {#'At':At, # too much space
            'wtot':np.sum(At),
            'passage': passage,
            'beta_range':beta_range,
            'motion': motion,
            'threshold': t,
            'Sdata': Sdata,
            'Smodel': Smodel,
            'dkl': dkl}

class MidpointNormalize(colors.Normalize):
    def __init__(self, vmin=None, vmax=None, midpoint=None, clip=False):
        self.midpoint = midpoint
        colors.Normalize.__init__(self, vmin, vmax, clip)

    def __call__(self, value, clip=None):
        # I'm ignoring masked values and all kinds of edge cases to make a
        # simple example...
        x, y = [self.vmin, self.midpoint, self.vmax], [0, 0.5, 1]
        return np.ma.masked_array(np.interp(value, x, y))

def get_density_percolation(passage, motion):
    import bct
    dfCrossley638 = pd.DataFrame(pickle.load(open('/home/carlo/workspace/entropy_analysis/notebooks/output/2019/processed/Crossley638/groups/data_forcellini_thresh_none.pkl','rb'))['forcellini'])
    thresh_abs_percolation = pd.read_csv('/home/carlo/workspace/entropy_analysis/data/forcellini/matrices/Crossley638/groups/conditions.txt', delimiter='\t')
    A = dfCrossley638[(dfCrossley638['passages']==passage ) & (dfCrossley638['motion']==motion)]['A'].values[0]
    t = thresh_abs_percolation[(thresh_abs_percolation['passages']==passage) & (thresh_abs_percolation['motion'] == motion)]['threshold'].values[0]
    dens = bct.density_und(bct.threshold_absolute(A,t))[0]
    return dens

if __name__ == '__main__':

    basedir = '/home/carlo/workspace/entropy_analysis/notebooks/output/2019/processed/'

    filename = basedir + '/Crossley638/groups/data_forcellini_thresh_none.pkl'
    df = pd.DataFrame(pickle.load(open(filename, 'rb'))['forcellini'])
    
    passages = pd.unique(df['passages'].values)
    motion = ['L', 'M', 'H']
    thresholds = np.arange(0.01,1,0.01) # np.linspace(0.01, 0.5, 21)
    #thresholds = np.linspace(0,1,50)
    # Define the whole experiment product space
    experiment = list(product(passages, motion, thresholds))
    
    # for passage, mot, thresh in product(passages, motion, thresholds):
    #     print(compute_dkl(passage, mot, thresh, beta_range=np.logspace(-3,3,50)))
    res = parmap.starmap(partial(compute_dkl), experiment, beta_range=np.logspace(-3,3,100),
                         pm_processes=31,
                         pm_chunksize=1,
                         pm_pbar=True)
    # saves the result
    pickle.dump(res,gzip.open('result_parmap_all_threshold_prop_beta_friday_15_march.pkl.gz','wb'))


    # To prepare the dataframe for generation of pictures that cut over the percolatio
    df['perc_density'] = [get_density_percolation(passage=df['passage'].values[i],motion=df['motion'].values[i]) for i in range(len(df))]
    thresholds = np.unique(df['threshold'].values) # these are all the thresholds as generated in the script
    df['threshold_from_perc'] = [ thresholds[np.where(thresholds>df['perc_density'].values[i])[0]] for i in range(len(df))]
    df['ix_threshold_from_perc'] = [ np.where(thresholds>df['perc_density'].values[i])[0] for i in range(len(df))]

    # # loads the result
    # df = pd.DataFrame(pickle.load(gzip.open('result_parmap_all_threshold_abs_beta.pkl.gz')))

    # import seaborn as sns
    # # Generate the plots
    # for motion in ['L','M','H']:
    #     grid = sns.FacetGrid(df[df['motion']==motion],col='passage',col_wrap=3, margin_titles=True)
    #     beta_range = np.logspace(-3,3,40)
    #     grid.map(lambda _x,_y, **kwargs : (plt.pcolormesh(beta_range,
    #                                                       pd.unique(df['threshold'])[0:],
    #                                                       np.array([ _x.values[i]/(beta_range*_y.values[i]) for i in range(len(thresholds))]),
    #                                                       cmap='Blues'),
    #                                        plt.xscale('log'),
    #                                        plt.colorbar(fraction=0.046, pad=0.04)),
    #              'dkl','wtot').add_legend()
    #     grid.fig.subplots_adjust(top=0.9)
    #     grid.fig.suptitle('Dkl Normalized by beta and total weight - Motion level -thresh-abs' + motion )
    #     grid.savefig('output/2019/processed/Crossley638/groups/dkl_cwerg_crossley_638_all_thresh_abs_div_beta_wtot_motion_' + motion + '.pdf')


    # for motion in ['L','M','H']:
    #     grid = sns.FacetGrid(df[df['motion']==motion],col='passage',col_wrap=3, margin_titles=True)
    #     beta_range = np.logspace(-3,3,40)
    #     grid.map(lambda _x,_y, **kwargs : (plt.pcolormesh(beta_range, pd.unique(df['threshold'])[0:], 
    #                                                      np.array([ _x.values[i]-_y.values[i] for i in range(len(thresholds))]),
    #                                                      cmap='coolwarm',
    #                                                      norm=MidpointNormalize(midpoint=0)),
    #                                     plt.xscale('log'),
    #                                     plt.colorbar(fraction=0.046, pad=0.04)) ,
    #              'Sdata','Smodel').add_legend()
    #     grid.fig.subplots_adjust(top=0.9)
    #     grid.fig.suptitle('Motion level ' + motion )
    #     grid.savefig('output/2019/processed/Crossley638/groups/entropy_diff_cwerg_crossley_638_all_thresh_abs_motion_' + motion + '.pdf')