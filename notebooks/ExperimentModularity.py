import os
from os.path import expanduser
home = expanduser("~")
import sys
import numpy as np
import pandas as pd
import scipy
import networkqit as nq
import networkx as nx
import bct
import pickle
# Additional utility functions
from multiprocessing import Pool, cpu_count
import pickle, gzip
import time
import progressbar
import functools

def optimize_modularity(x):
    rep = 16
    res = [bct.community_louvain(x) for r in range(rep)]
    imaxq = np.argmax([r[1] for r in res ])
    return res[imaxq]

if __name__=='__main__':
  T = []
  df = None
  pool = Pool(16)
  for thresh in progressbar.progressbar(np.linspace(0.025,0.25,10)):
      thresh_str = '%.4f' % thresh
      del df
      df = pickle.load(open('data_forcellini_thresh_'+ thresh_str +'.pkl','rb'))['forcellini']
      for passages in pd.unique(df.passages):
          for motion in pd.unique(df.motion):
              print('thresh=%f passages=%d motion=%s'% (thresh,passages,motion))
              W = df[np.logical_and(df.passages==passages, df.motion==motion)]['A'].iloc[0]

              res = pool.map(optimize_modularity,[W]*cpu_count()) # must map the iterable
              memb,q = res[np.argmax([r[1] for r in res ])]
              memb = nq.reindex_membership(memb)
              ersw,ersnormw = nq.comm_mat(W,memb)
              ers,ersnorm = nq.comm_mat((W>0).astype(float),memb)
              
              T.append({'passages':str(passages),
                        'thresh':thresh_str,
                        'motion':motion,
                        'qmemb':memb,
                        'W':W,
                        'q':q,
                        'qersw': ersw,
                        'qersnormw':ersnormw,
                        'qers': ers,
                        'qersnorm':ersnorm })
              dfinfo = pd.DataFrame(T)
              pickle.dump(dfinfo, gzip.open('output/processed/wsbm_dfinfo_mod.pkl.gz', "wb"), protocol=-1)
  dfinfo = pd.DataFrame(T)
  pickle.dump(dfinfo, gzip.open('output/processed/wsbm_dfinfo_mod.pkl.gz', "wb"), protocol=-1)

