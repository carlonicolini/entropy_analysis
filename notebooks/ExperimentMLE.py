#!/usr/bin/python3
import os
from os.path import expanduser
home = expanduser("~")
import sys
sys.path.append(home + '/workspace/networkqit')

# This is to set a single thread to each scipy optimization worker
os.environ["MKL_NUM_THREADS"] = "1"
os.environ["NUMEXPR_NUM_THREADS"] = "1"
os.environ["OMP_NUM_THREADS"] = "1"

import networkqit as nq
import numpy as np
import pandas as pd
import scipy
from scipy.io import loadmat
import pickle
# Additional utility functions
import pickle
import gzip
from tqdm import tqdm
from functools import partial
from itertools import product
from multiprocessing import Pool, cpu_count
import parmap
import logging
logging.basicConfig(level=logging.INFO)
logger = logging.getLogger(__name__)
from time import gmtime, strftime


def solve_mle(*args, **kwargs):
    (passages, motion, thresh) = args
    #thresh_str = '%.4f' % thresh
    thresh_str = thresh
    G = kwargs['all_W'][(thresh, passages, motion)]
    t = G[np.nonzero(G)].min()
    G = (G>0).astype(float)
    #M = nq.CWTERG(N=len(G),threshold=t)
    M = nq.ErdosRenyi(N=len(G))#,threshold=t)
    sol = M.fit(G, method='MLE', verbose=0)
    #sol = M.fit(G, x0=sol['x'], verbose=0, maxiter=200, gtol=1E-9, method='saddle_point')
    Ls = nq.normalized_graph_laplacian(M.sample_adjacency(sol['x'], batch_size=2))
    beta_range = np.logspace(-3,3,100)
    Dkl = np.array([nq.batch_relative_entropy(Lobs=nq.normalized_graph_laplacian(G), Lmodel=Ls, beta=b) for b in beta_range])
    out = {'passages': passages,
           'motion': motion,
           'thresh_prop': thresh_str,
           'thresh_abs': t,
           'W': G,
           'sol':sol,
           'loglikelihood': M.loglikelihood(G,sol['x']), # already included in 'sol'
           'dkl': Dkl,
           'error': M.saddle_point(G,sol['x']),
           'beta_range':beta_range
           }
    return out

# def run_subject(prefix, subject):
#     logger.info('Subject %s analysis started at %s' % (str(subject),strftime("%Y-%m-%d %H:%M:%S", gmtime()) ))
#     T = []
#     passages = [0, 1, 5, 6, 8, 9, 24, 27, 85]
#     motion = ['L', 'M', 'H']
#     thresholds = [0.05, 0.1, 0.15, 0.20]
#     # Preload the matrix W and the graph g into separate dictionaries
#     # This is done to save reading from disk multiple times
#     all_W = {}
#     for thresh in thresholds:
#         thresh_str = '%.4f' % thresh
#         df = pickle.load(open(prefix + thresh_str + '.pkl', 'rb'))['forcellini']
#         for passage in passages:
#             for mot in motion:
#                 all_W[(thresh, passage, mot)] = df[np.logical_and(passage == df.passages, df.motion == mot)]['A'].iloc[0]
#     del df  # to save memory
#     experiment = list(product(passages, motion, thresholds))
#     logger.info('Subject %s parameters generation started at %s' % (str(subject),strftime("%Y-%m-%d %H:%M:%S", gmtime()) ))
#     logger.info('Subject %s parallel processing started at %s' % (str(subject),strftime("%Y-%m-%d %H:%M:%S", gmtime()) ))
#     # Run the parallel simulation
#     res = parmap.starmap(partial(solve_mle, all_W=all_W), experiment, pm_processes=32, pm_chunksize=1, pm_pbar=True)
#     logger.info('Subject %s parallel processing finished at %s' % (str(subject),strftime("%Y-%m-%d %H:%M:%S", gmtime()) ))
#     logger.info('Subject %s file dump started at %s' % (str(subject),strftime("%Y-%m-%d %H:%M:%S", gmtime()) ))
#     pickle.dump(res, gzip.open('output/2019/processed/BNA242/subjwise/mle_dfinfo_subj_%d.pkl.gz' % subj, 'wb'), protocol=-1)
#     logger.info('Subject %s finished at %s' % (str(subject),strftime("%Y-%m-%d %H:%M:%S", gmtime()) ))

def run_group(filename):
    logger.info('Analysis started at %s' % (strftime("%Y-%m-%d %H:%M:%S", gmtime()) ))
    T = []
    passages = [0, 1, 5, 6, 8, 9, 24, 27, 85]
    motion = ['L', 'M', 'H']
    thresholds = [''] #['0.0500', '0.1000', '0.1500', '0.2000', '0.2500'] #, '0.3000','0.3500','0.4000']    # Preload the matrix W and the graph g into separate dictionaries
    # This is done to save reading from disk multiple times
    all_W = {}
    for thresh in thresholds:
        #thresh_str = '%.4f' % thresh
        df = pickle.load(open(filename + thresh + '.pkl', 'rb'))['forcellini']
        for passage in passages:
            for mot in motion:
                all_W[(thresh, passage, mot)] = df[np.logical_and(passage == df.passages, df.motion == mot)]['A'].iloc[0]
    del df  # to save memory
    experiment = list(product(passages, motion, thresholds))
    logger.info('parameters generation started at %s' % (strftime("%Y-%m-%d %H:%M:%S", gmtime()) ))
    logger.info('parallel processing started at %s' % (strftime("%Y-%m-%d %H:%M:%S", gmtime()) ))
    # Run the parallel simulation
    res = parmap.starmap(partial(solve_mle, all_W=all_W), experiment, pm_processes=32, pm_chunksize=1, pm_pbar=True)
    logger.info('parallel processing finished at %s' % (strftime("%Y-%m-%d %H:%M:%S", gmtime()) ))
    logger.info('file dump started at %s' % (strftime("%Y-%m-%d %H:%M:%S", gmtime()) ))
    pickle.dump(res, gzip.open('output/2019/processed/Crossley638/spars/mle_dfinfo_bin_norm_erg.pkl.gz', 'wb'), protocol=-1)
    logger.info('finished at %s' % (strftime("%Y-%m-%d %H:%M:%S", gmtime()) ))

if __name__ == '__main__':
    run_group('output/2019/processed/Crossley638/spars/data_forcellini_thresh_none')
    #for subj in range(39):
    #subj=1
    #run_subject('output/2019/processed/BNA/subjwise/data_subj_%d_thresh_' % subj, subj)
