#!/usr/bin/python3
import numpy as np
import graph_tool.all as gt

def to_graph_tool_OLD(adj):
    print('ERROR this function does not work as intended')
    g = gt.Graph(directed=False)
    eprop = g.new_edge_property('double')
    g.edge_properties['weight'] = eprop
    nnz = np.nonzero(np.triu(adj))
    g.add_edge_list(np.hstack([np.transpose(nnz),adj[nnz[0]]]), eprops=eprop)
    return g

def to_graph_tool_slow(adj):
    g = gt.Graph(directed=False)
    edge_weights = g.new_edge_property('double')
    g.edge_properties['weight'] = edge_weights
    num_vertices = len(adj)
    for i in range(0,num_vertices):
        for j in range(i+1,num_vertices):
            if adj[i,j]!=0:
                e = g.add_edge(i,j)
                edge_weights[e] = adj[i,j]
    return g

def to_graph_tool(adj):
    g = gt.Graph(directed=False)
    edge_weights = g.new_edge_property('double')
    g.edge_properties['weight'] = edge_weights
    nnz = np.nonzero(np.triu(adj,1))
    nedges = len(nnz[0])
    g.add_edge_list(np.hstack([np.transpose(nnz),np.reshape(adj[nnz],(nedges,1))]),eprops=[edge_weights])
    return g

def add_vertex_membership_property(g,memb,color=None, colormap=None):
    v_memb = g.new_vertex_property("int32_t") # define vertex membership property
    g.vertex_properties['memb'] = v_memb
    for i in range(0,len(memb)):
        v_memb[i] = memb[i]
    
    vcolor = g.new_vertex_property('vector<double>')
    def rescale(x):
        return (x-np.min(x))/(np.max(x)-np.min(x))
    color_memb = rescale(memb)
        
    for i in g.vertices():
        vcolor[i] = colormap(color_memb[int(i)])

    g.vertex_properties['color'] = vcolor
    return g

def add_edge_gradient_property(g,memb,colormap):
    egradient = g.new_edge_property('vector<double>') # define edge gradient property
    g.edge_properties['egradient'] = egradient
    le = list(g.edges())
    alpha = 1
    elist = list(g.ep.weight)
    max_weight = np.max(elist)
    def rescale(x):
        #x = 1-x
        return (x-np.min(x))/(np.max(x)-np.min(x))
    color_memb = rescale(memb)
    for i,(e0,e1) in enumerate(g.edges()):
        rgb0 = colormap(color_memb[int(e0)])
        rgb1 = colormap(color_memb[int(e1)])
        egradient[le[i]] = [0, *rgb0, 1, *rgb1]
    return g

def add_vertex_names(g, vertex_names=None):
    names = g.new_vertex_property("string")
    if vertex_names is None:
        for i in g.vertices():
            names[i]=str(i)
    else:
        for i in g.vertices():
            names[i]=vertex_names[i]
    g.vertex_properties['name'] = names
    return g

    

def circo_edge_bundle(g, memb, colormap=None, **kwargs):
    cmap = colormap
    if colormap is None:
        cmap = plt.get_cmap('tab10')
    g = add_vertex_membership_property(g,memb,memb,cmap)
    g = add_edge_gradient_property(g,memb,cmap)
    # Create a fake Nested block state with 2 levels.
    # First level is super-node 0, second level is node membership
    vertices = list(range(0,g.num_vertices()))
    bs = [gt.PropertyArray(memb, vertices),gt.PropertyArray([0],[0])]
    state = gt.NestedBlockState(g, bs)
    t = gt.get_hierarchy_tree(state)[0]
    tpos = pos = gt.radial_tree_layout(t, t.vertex(t.num_vertices() - 1), weighted=True)
    cts = gt.get_hierarchy_control_points(g, t, tpos)
    pos = g.own_property(tpos)
    b = state.levels[0].b
    min_weight = np.min(list(g.ep.weight)) + 0.1
    max_weight = np.max(list(g.ep.weight))
    gt.graph_draw(g,
                  pos=pos,
                  vertex_color=g.vp.color,
                  #vertex_text=g.vp.memb,
                  vertex_fill_color=g.vp.color,
                  vertex_anchor=0,
                  edge_gradient = g.ep.egradient,
                  edge_pen_width=gt.prop_to_size(g.ep.weight, mi=min_weight, ma=max_weight, power=0.5),
                  edge_control_points=cts,
                  **kwargs)
