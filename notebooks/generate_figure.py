import matplotlib as mpl
mpl.use('Agg')
import sys
import numpy as np
import pandas as pd
import networkqit as nq
import bct
import seaborn as sns
import matplotlib.pyplot as plt
import pickle

from nilearndrawmembership import draw_parcellation_multiview

# important run this code with export PYTHONHASHSEED=0 from the shell
if __name__=='__main__':

    def save_plot(memb):
        template = '/home/carlo/workspace/communityalg/data/template_638.nii'
        surf_left = '/home/carlo/workspace/Brainet2017/Data/SurfTemplate/BrainMesh_ICBM152Left_smoothed.nv'
        surf_right = '/home/carlo/workspace/Brainet2017/Data/SurfTemplate/BrainMesh_ICBM152Right_smoothed.nv'
        memb_reindex = np.array(nq.reindex_membership(memb)) + 1
        filename = '/tmp/brain' + str(hash(str(memb))) + '.png' # to be unique    
        from pathlib import Path
        if not Path(filename).exists(): # create it
            draw_parcellation_multiview(template, surf_left, surf_right, memb_reindex, output_file = filename)
        else:
            print('skipped generation of ' + filename + ' already existing')
        return plt.figure()

    def show_plot(memb):
        filename = '/tmp/brain' + str(hash(str(memb))) + '.png' # to be unique    
        fig = plt.imshow(plt.imread(filename), interpolation='none')
        plt.grid(False)
        plt.axis('off')
        return fig

    B = int(sys.argv[1])
    thresh = sys.argv[2]
    passages_order = [9,27,0,1,8,6,24,5,85]
    motion_order = ['L','M','H']

    df = pickle.load(open('output/processed/wsbm_dfinfo_lognormal.pkl','rb'))
    df = df[(df.B==B) & (df.thresh==thresh)]
    sns.set(style="ticks")
    grid = sns.FacetGrid(df, row='passages', col='motion',col_order=['L','M','H'], margin_titles=True, sharey=True, aspect=1, height=10)
    

    # first generate all files, returning an empty figure
    #grid.map(lambda _memb, **kwargs: save_plot(_memb.values[0]), 'memb')
    # then load the figures in each slot
    grid.map(lambda _memb, **kwargs: show_plot(_memb.values[0]), 'memb')
    grid.savefig('output/processed/wsbm_dfinfo_brain_B_%d_thresh_%s.png' % (B,thresh), dpi=200)
