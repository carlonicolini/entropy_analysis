import os
from os.path import expanduser
home = expanduser("~")
import sys
sys.path.append(home + '/workspace/networkqit')
import numpy as np
import pandas as pd
import scipy
from scipy.io import loadmat
import networkqit as nq
import networkx as nx
import bct
import matplotlib.pyplot as plt
import pickle
import parmap
from functools import partial
from itertools import product
import seaborn as sns
plt.style.use('ggplot')

def compute_mle(*args,**kwargs):
    Model, thresh = args[0],args[1]
    beta_range = kwargs['beta_range']
    Gpatient = bct.threshold_proportional(df[df['condition']=='patient']['matrix'].values[0], thresh)
    Gcontrol = bct.threshold_proportional(df[df['condition']=='control']['matrix'].values[0], thresh)
    N = Gpatient.shape[0] # 638
    Mpatient = Model(N=len(Gpatient),threshold=Gpatient[np.nonzero(Gpatient)].min())
    solpatient = Mpatient.fit(Gpatient, method='MLE', maxiter=10000)
    # refine solution a bit
    solpatient = Mpatient.fit(Gpatient, method='saddle_point', x0=solpatient['x'], maxiter=50)
    
    Mcontrol = Model(N=len(Gcontrol),threshold=Gcontrol[np.nonzero(Gcontrol)].min())
    solcontrol = Mcontrol.fit(Gcontrol, method='MLE', maxiter=10000)
    # refine solution a bit
    solcontrol = Mcontrol.fit(Gcontrol, method='saddle_point', x0=solcontrol['x'], maxiter=50)
        
    # Compute Dkl
    if Model is nq.CWTECM:
        I = 1
        modelname = 'CWTECM'
    elif Model is nq.CWTERG:
        I = 1-np.eye(N)
        modelname = 'CWTERG'
        
    # Compute spectral entropies
    Spatient = nq.batch_compute_vonneumann_entropy(L=nq.graph_laplacian(Gpatient),beta_range=beta_range)
    Spatientrandom = nq.batch_compute_vonneumann_entropy(L=nq.graph_laplacian(Mpatient.expected_weighted_adjacency(solpatient['x'])*I), beta_range=beta_range)
    Scontrol = nq.batch_compute_vonneumann_entropy(L=nq.graph_laplacian(Gcontrol),beta_range=beta_range)
    Scontrolrandom = nq.batch_compute_vonneumann_entropy(L=nq.graph_laplacian(Mcontrol.expected_weighted_adjacency(solcontrol['x'])*I), beta_range=beta_range)

    dkl_patient = nq.batch_beta_relative_entropy(Lobs=nq.graph_laplacian(Gpatient),
                                                 Lmodel=nq.graph_laplacian(Mpatient.expected_weighted_adjacency(solpatient['x'])*I),
                                                 beta_range=beta_range)
    dkl_control = nq.batch_beta_relative_entropy(Lobs=nq.graph_laplacian(Gcontrol),Lmodel=nq.graph_laplacian(Mcontrol.expected_weighted_adjacency(solcontrol['x'])*I),
                                                 beta_range=beta_range)
    return {'thresh_prop': thresh,
            'Mpatient': Mpatient,
            'solpatient': solpatient,
            'Mcontrol': Mcontrol,
            'solcontrol': solcontrol,            
            'dkl_patient': dkl_patient,
            'wtotpatient': Gpatient.sum(),
            'wtotcontrol': Gcontrol.sum(),
            'dkl_control': dkl_control,
            'Spatient': Spatient,
            'Spatientrandom':Spatientrandom,
            'Scontrol': Scontrol,
            'Scontrolrandom':Scontrolrandom,
            'modelname': modelname,
            'beta_range':beta_range
           }

if __name__=='__main__':
    os.environ["MKL_NUM_THREADS"] = "1"
    os.environ["NUMEXPR_NUM_THREADS"] = "1"
    os.environ["OMP_NUM_THREADS"] = "1"
    res = pickle.load(open('output/2019/processed/schizo/data_schizo_raw_fisher_averages.pkl','rb'))
    df = pd.DataFrame(res['bordier']['data'])

    experiment = list(product([nq.CWTERG],np.arange(0.01, 0.21, 0.01)))
    out = parmap.starmap(partial(compute_mle), iterables=experiment, beta_range=np.logspace(-3, 3, 100),
                         pm_processes=30, pm_chunksize=1, pm_pbar=True)
    dfout = pd.DataFrame(out)

    unnested_lst = []
    for col in dfout.columns:
        unnested_lst.append(dfout[col].apply(pd.Series).stack())
    result = pd.concat(unnested_lst, axis=1, keys=dfout.columns).fillna(method='ffill')
    pickle.dump([dfout,result],open('output/2019/processed/schizo/solution_cwterg_thresholds_1_to_21_percent_schizo.pkl','wb'))

    ## PLOTTING PART ##
    # Plot the entropies
    # grid = sns.FacetGrid(result, col='thresh_prop',col_wrap=4,sharex=False, height=4,legend_out=True)
    # grid.map(plt.semilogx, 'beta_range','Spatient',color=sns.color_palette()[0],linestyle='-',label='Patient')
    # grid.map(plt.semilogx, 'beta_range','Spatientrandom',color=sns.color_palette()[0],linestyle='-.',linewidth=0.5,label='Patient randomized')

    # grid.map(plt.semilogx, 'beta_range','Scontrol',color=sns.color_palette()[1],linestyle='-',label='Control')
    # grid.map(plt.semilogx, 'beta_range','Scontrolrandom',color=sns.color_palette()[1],linestyle='-.',linewidth=0.5,label='Control randomized').add_legend()
    # grid.set_axis_labels('$\\beta$','$S$')
    # grid.savefig('output/2019/processed/schizo/entropies_schizo_patient_controls_1_to_21_percent_cwterg.pdf',bbox='tight',dpi=200)

    # # Plot the Dkl
    # grid = sns.FacetGrid(result, col='thresh_prop', col_wrap=4,sharex=True, sharey=True,height=4,legend_out=True)
    # grid.map(plt.semilogx,'beta_range','dkl_patient',label='Patient',color=sns.color_palette()[0])
    # grid.map(plt.semilogx,'beta_range','dkl_control',label='Control',color=sns.color_palette()[1]).add_legend()
    # grid.set_axis_labels('$\\beta$','$S(\\rho \\| \\sigma)$')
    # grid.savefig('output/2019/processed/schizo/dkl_schizo_patient_controls_1_to_21_percent_cwterg.pdf', bbox='tight')